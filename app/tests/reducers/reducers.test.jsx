var expect = require('expect');
var df = require('deep-freeze-strict');
var reducers = require('reducers');

describe('Reducers', () => {
  describe('searchTextReducer', () => {
    it('should set searchText', () => {
      var action = {
        type: 'SET_SEARCH_TEXT',
        searchText: 'dog'
      };
      var res = reducers.searchTextReducer(df(''), df(action));
      expect(res).toEqual(action.searchText);
    });
  });

  describe('showCompletedReducer', () => {
    it('should toggle show completed', () => {
      var action = {
        type: 'TOGGLE_SHOW_COMPLETED'
      };
      var res = reducers.showCompletedReducer(df(true), df(action));
      expect(res).toEqual(false);
    });
  });

  describe('authReducer', () => {
    it('should store uid user on login', () => {
      const action = {
        type: 'LOGIN',
        uid: '123abc'
      };
      var res = reducers.authReducer(undefined, df(action));
      expect(res).toEqual({
        uid: action.uid
      })
    });

    it('should wipe auth on logout', () => {
      const authData = {
        uid: '123abc'
      }
      const action = {
        type: 'LOGOUT'
      };
      var res = reducers.authReducer(df(authData), df(action));
      expect(res).toEqual({});
    })
  });

  describe('todosReducer', () => {
    it('should add new todo', () => {
      var action = {
        type: 'ADD_TODO',
        todo: {
          id: '123',
          text: 'Walk the dog'  ,
          completed: false,
          createdAt: 12345
        }

      };
      var res = reducers.todosReducer(df([]), df(action));
      expect(res.length).toEqual(1);
      expect(res[0]).toEqual(action.todo);
    });

    it('should update todo', () => {
      var todos = [
        {
          id: 1,
          text: 'Dummy 1',
          completed: false,
          createdAt: 0,
          completedAt: undefined

        },
        {
          id: 2,
          text: 'Dummy 2',
          completed: true,
          createdAt: 10,
          completedAt: 60
        }
      ];

      var updates = {
        completed: true,
        completedAt: 100
      };
      var action = {
        type: 'UPDATE_TODO',
        id: todos[0].id,
        updates
      };
      var res = reducers.todosReducer(df(todos), df(action));
      expect(res[0].completed).toEqual(updates.completed);
      expect(res[0].completedAt).toEqual(updates.completedAt);
      expect(res[0].text).toEqual(todos[0].text);

      var action = {
        type: 'UPDATE_TODO',
        id: todos[1].id,
        updates: {
          completed: false,
          completedAt: null
        }
      };
      res = reducers.todosReducer(df(todos), df(action));
      expect(res[1].completed).toEqual(false);
      expect(res[1].completedAt).toNotExist();
    });

    it('should add existing todos', () => {
      var todos = [{
        id: '111',
        text: 'Something',
        completed: false,
        completedAt: undefined,
        createdAt: 33000
      }];

      var action = {
        type: 'ADD_TODOS',
        todos
      };

      var res = reducers.todosReducer(df([]), df(action));
      expect(res.length).toEqual(1);
      expect(res[0]).toEqual(todos[0]);
    });

    it('should wipe todos on logout', () => {
      var todos = [{
        id: '111',
        text: 'Something',
        completed: false,
        completedAt: undefined,
        createdAt: 33000
      }];

      var action = {
        type: 'LOGOUT'
      };

      var res = reducers.todosReducer(df(todos), df(action));
      expect(res.length).toEqual(0);
    });
  });
});
